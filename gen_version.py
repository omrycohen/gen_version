import os
import git
import pathlib


class GitVersion:

    _NO_DIFF = "No diff!"
    _NO_UNTRACKED_FILES = "No Untracked Files!"
    _DIFF_TEMPLATE = "***** DIFF\n\n{}\n\n***** UNTRACKED FILES\n\n{}"
    _INDENT = "    "

    def __init__(self, project_path=None):

        if project_path is None:
            project_path = str(pathlib.Path(__file__).parent.parent.absolute())

        repo = git.Repo(project_path)
        assert not repo.bare, f"{project_path!r} is not a git repository!"

        self.project_url = repo.git.config("--get", "remote.origin.url")
        
        self.is_HEAD_detached = repo.head.is_detached
        self.project_hash = repo.head.commit.hexsha
        self.git_diffs, self.has_diff, self.has_untracked_files = GitVersion._get_recursive_diff(repo)

    def __str__(self):

        catchyPhrase = "##################################"

        string =  f"{catchyPhrase} GIT VERSION {catchyPhrase}\n\n"
        string += f"{catchyPhrase} Project:\n\n{self.project_url}\n{self.project_hash}\n\n"
        string += f"{catchyPhrase} Dirty:\n\n{'Yes' if self.has_diff else 'No'}\n\n\n"
        string += f"{catchyPhrase} Has Untracked Files:\n\n{'Yes' if self.has_untracked_files else 'No'}\n\n\n"
        string += f"{catchyPhrase} Diff:\n\n {self.git_diffs}\n\n"
        string += f"{catchyPhrase} GIT VERSION END {catchyPhrase}\n\n"

        return string

    @staticmethod
    def indent(string : str):
        return GitVersion._INDENT + string.replace("\n", "\n" + GitVersion._INDENT)

    @staticmethod
    def _get_recursive_diff(repo: git.Repo):

        # make sure the used commit is existent in the remote
        remotes = repo.git.branch("-r", "--contains", repo.head.commit.hexsha)
        assert (remotes.startswith("  ") and any(s.startswith("origin/") for s in remotes[2:].split("\n  "))),\
             f"must push {repo.working_dir!r} to GitLab! otherwise the VERSION is not reproducible"

        diff = repo.git.diff(repo.head, repo.working_dir)

        if diff is "":
            diff = GitVersion._NO_DIFF
            has_diff = False
        else:
            has_diff = True

        # make sure index is empty
        if len(repo.index.diff("HEAD")) != 0:
            raise Exception(f"Index of git repository {repo.working_dir!r} is not empty, cannot calculate diff. Please unstage all files!")

        # calc Untracked Files
        untracked_files_list = [filename for filename in repo.git.ls_files("--other", "--exclude-standard").split("\n") if filename != ""]
        has_untracked_files = len(untracked_files_list) > 0
        if has_untracked_files:
            untracked_diff = ""
            for filename in untracked_files_list:
                with open(os.path.join(repo.working_dir, filename), "r") as f:
                    try:
                        untracked_diff += f"\n file name: {filename!r},\nContent:\n{{\n{GitVersion.indent(f.read())}\n}}\n"
                    except:
                        raise Exception(f"Project {repo.working_dir!r} contains an untracked binary file {filename!r}.")
        else:
            untracked_diff = GitVersion._NO_UNTRACKED_FILES

        # calc diff
        diff = GitVersion._DIFF_TEMPLATE.format(diff, untracked_diff)

        for submodule in repo.submodules:
            submodule_repo = git.Repo(submodule.abspath)
            submodule_diff, submodule_has_diff, submodule_has_untracked_files = GitVersion._get_recursive_diff(submodule_repo)
            submodule_diff = GitVersion.indent(submodule_diff)
            diff += f"\n\n### diff of submodule {submodule.path!r} w.r.t commit {submodule_repo.head.commit.hexsha}:\n\n{submodule_diff}"
            has_diff = has_diff or submodule_has_diff
            has_untracked_files = has_untracked_files or submodule_has_untracked_files

        return diff, has_diff, has_untracked_files
